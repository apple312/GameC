﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.BasicWeaponMoves
{
	class SwordBlock : Skill
	{
        // block will give 20 stamina to player
        public SwordBlock() : base("Sword Block", -20, 7)
        {
            PublicName = "Sword block gives you back 20 stamina in this turn.";
            RequiredItem = "Sword";
        }
        public override List<StatPackage> BattleMove(Player player)
        {
            StatPackage response = new StatPackage("none", 0, "I am blocking, getting back stamina"); 
            return new List<StatPackage>() { response };
        }
    }
}
