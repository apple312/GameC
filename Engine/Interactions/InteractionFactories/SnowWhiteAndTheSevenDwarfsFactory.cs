﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Engine.Interactions.Added;

namespace Game.Engine.Interactions.InteractionFactories
{
	class SnowWhiteAndTheSevenDwarfsFactory : InteractionFactory
	{
		private bool created = false;
		public static List<string> sevenDwarfsNames = new List<string>
		{
			"interaction0009",
			"interaction0010",
			"interaction0011",
			"interaction0012",
			"interaction0013",
			"interaction0014",
			"interaction0015"
		};
		public List<Interaction> CreateInteractionsGroup(GameSession parentSession)
		{
			List<Interaction> tmpList = new List<Interaction>();
			if (created) return tmpList;    //return empty, because only one in game is possible

			WitchEncounter witch = new WitchEncounter(parentSession);
			SnowWhiteEncounter snowWhite = new SnowWhiteEncounter(parentSession);
			MirrorEncounter mirror = new MirrorEncounter(parentSession);

			foreach (string name in sevenDwarfsNames)
			{
				DwarfEncounter dwarf = new DwarfEncounter(parentSession, name, snowWhite, mirror); // snow White and mirror will know that you met this dwarf
				snowWhite.Subscribe(dwarf);     //dwarf will know that you visited snowwhite earlier
				mirror.Subscribe(dwarf);
				tmpList.Add(dwarf);
			}
			
			witch.Subscribe(mirror);			//mirror will know about your visits to witch
			snowWhite.Subscribe(mirror);        //mirror will know abot your visits to snowWhite
			
			tmpList.Add(witch);
			tmpList.Add(mirror);
			tmpList.Add(snowWhite);

			created = true;
			return tmpList;
		}
	}
}
