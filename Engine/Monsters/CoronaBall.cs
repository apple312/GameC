﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Monsters
{
    [Serializable]
	class CoronaBall : Monster
	{
        // you can fight with more or less danger version of Corona
        private bool danger = false;
        private int chronicity = 0; // it will increase during fight and if will be over 5 it will have some effects
        public CoronaBall(int coronaLevel)
        {
            if (Index.RNG(-1, 2) < 0) danger = true; // I guess this will be more probably to get easy one
            else danger = false;

            Armor = 0;
            Precision = 50;
            MagicPower = 0;
            XPValue = 20 + coronaLevel; //stays the same, you are just unlucky to get dangerous version
            Name = "monster0003";
            if (danger == false)
            {
                BattleGreetings = "I am asymptomatic one."; // sometimes corona is not so dangerous
                Stamina = 50;
                Health = 40 + 3 * coronaLevel;
                Strength = 10 + coronaLevel;
            }
            else
            {
                BattleGreetings = "I am dangerous version, special for you.";
                Stamina = 60;
                Health = 90 + 6 * coronaLevel;
                Strength = 10 + 2 * coronaLevel;
            }
        }

        public override List<StatPackage> BattleMove()
		{
            if (Stamina >= 10)
            {
                Stamina -= 10;
                chronicity++; // coronna getting more and more dangerous during long fight
                // a simple attact gives health damage if danger 6 + strength, if not danger just strength
                if (chronicity > 3)
                {
                    return new List<StatPackage>() 
                    { 
                        new StatPackage("breathing problems", Convert.ToInt32(danger) * 6 + Strength, "Corona attacts ( " + (Convert.ToInt32(danger) * 6 + Strength) + " by breaching problems)" ),
                        new StatPackage("complications", 1, 1, 1, 1, 1, "Affecting your health and state"),
                    };
                }
                else
                {
                    return new List<StatPackage>() { new StatPackage("breathing problems", Convert.ToInt32(danger) * 6 + Strength, "Corona attacts. ( " + (Convert.ToInt32(danger) * 6 + Strength) + " by breaching problems)" )};
                }
            }
            else
            {
                return new List<StatPackage>() { new StatPackage("none", 0, "Corona has not enough energy to attack!") };
            }
        }

        public override void React(List<StatPackage> packs)
        {
            base.React(packs);
            // danger version getting back some stamina every turn
            if (danger)
                stamina += 3;
        }
    }
}
