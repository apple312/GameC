﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Monsters
{
    [Serializable]
    class DeamonBoss : Monster
    {
        private bool defenceFlag = false;
        public DeamonBoss(int deamonBossLevel)
        {
            Health = 300 + 5 * deamonBossLevel;
            Strength = 70 + deamonBossLevel;
            Armor = 45;
            Precision = 60;
            MagicPower = 35;
            Stamina = 60;
            XPValue = 375 + 2 * deamonBossLevel;
            Name = "monster0005";
            BattleGreetings = " I am boss deamon, you are probably dead!";
        }
        public override List<StatPackage> BattleMove()
        {
            if (Stamina > 0)
            {
                // in move after defence armor should be back to normal
                if (defenceFlag == true)
                {
                    Armor = 45;
                    defenceFlag = false;
                }

                Stamina -= 20;
                // if deamon is lucky it gets critical attack and decrease players armor and magic power
                if (Index.RNG(0, 100) < precision)
                {
                    return new List<StatPackage>()
                    {
                        new StatPackage("magic critical stab", 2 * Strength + MagicPower, 10, 10, 0, MagicPower * 2,
                                        "Magic Crittical hit (" + (2 * Strength + MagicPower) + "damage) ( " + 10 + " strength decrease) ( " + 10 + " armor decrease) ( " + MagicPower * 2 + " magic power decrease)" )
                    };
                }
                else
                {
                    return new List<StatPackage>()
                    {
                        new StatPackage("magic stab", 2 * Strength + MagicPower,
                                        "Magic hit (" + (2 * Strength + MagicPower) + "damage)" )
                    };
                }

            }
            else
            {
                // when deamon will not have enough stamina it would do defence and rest
                defenceFlag = true;
                Armor += 50;
                Stamina += 30;
                return new List<StatPackage>() { new StatPackage("none", 0, "Demon in defence, gaining armor and stamina...") };
            }
        }
        public override void React(List<StatPackage> packs)
        {
            foreach (StatPackage pack in packs)
            {
                if (pack.HealthDmg - Armor > 0)
                    Health -= (pack.HealthDmg - Armor); 
                Strength -= pack.StrengthDmg;
                Armor -= pack.ArmorDmg;
                Precision -= pack.PrecisionDmg;
                MagicPower -= pack.MagicPowerDmg / 2; // boss deamon can controll magic better
            }
        }
    }
}
