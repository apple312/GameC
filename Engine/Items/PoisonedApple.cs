﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Items
{
	class PoisonedApple : Item
	{
		public PoisonedApple() : base("item0012")
		{
			PublicName = "Poisoned Apple";
		}
	}
}
