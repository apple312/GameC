﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Items
{
    [Serializable]
    class ShortSword : Sword
	{
        public ShortSword() : base("item0010")
        {
            StrMod = 7;
            PrMod = 30;
            GoldValue = 30;
            StaMod = 5;    // short sword is so easy to use that you will have more stamina left
            PublicName = "Short Sword";
            PublicTip = "Short sword makes great combo with other swords.";
        }

        public override StatPackage ModifyOffensive(StatPackage pack, List<string> otherItems)
        {
            foreach (var item in otherItems)
            {
                if (ListOfItems.ListOfSwords.Contains(item))
                {
                    pack.HealthDmg = pack.HealthDmg * 2;
                    break;
                }
            }
            return pack;
        }
    }
}
