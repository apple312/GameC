﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Engine.Items;
using Game.Engine.Monsters;

namespace Game.Engine.Interactions.Added
{
	class WitchEncounter : ImageInteraction, IMeetingObserver
	{
		private bool helping = false;
		private bool visited = false;
		private List<IMeetingObserver> observers = new List<IMeetingObserver>();
		private List<string> charactersVisitedByPlayer = new List<string>();
		public WitchEncounter(GameSession ses, params IMeetingObserver[] observers) : base(ses)
		{
			Enterable = false;
			Name = "interaction0008";
			displayedImageName = "interaction0008display";
			foreach (IMeetingObserver observer in observers)
			{
				this.observers.Add(observer);
			}
		}

		public void Receive(string name)
		{
			charactersVisitedByPlayer.Add(name);
		}

		public void RunObservers(string textToSend)
		{
			foreach (IMeetingObserver observer in observers)
			{
				observer.Receive(textToSend);
			}
		}

		public void Unsubscribe(IMeetingObserver observer)
		{
			observers.Remove(observer);
		}

		public void Subscribe(IMeetingObserver observer)
		{
			observers.Add(observer);
		}

		protected override void RunContent()
		{
			string answer1 = "Suree, I like money";
			string answer2 = "No, I dont like witches";
			if (visited == false)
			{
				parentSession.SendText("Hellooo.. Do you need some money traveler?\nI need your help in someting..");
			}
			else
			{
				if (helping == true)
				{
					parentSession.SendText("Did you did your mission already?");
					answer1 = "Yes";
					answer2 = "Noo, not yet.";
				}
				else
				{
					parentSession.SendText("BE GONE!!!");
					parentSession.FightThisMonster(new DeamonBoss(1000)); // it probably will kill player
					return; // !!!! moze trzeba to wywalic					
				}
			}

			parentSession.SendText(answer1 + " (press 1)");
			parentSession.SendText(answer2 + " (press 2)");
			parentSession.SendText("ENTER to leave");
			while (true)
			{
				string key = parentSession.GetValidKeyResponse(new List<string>() { "Return", "1", "2" }).Item1;
				if (key == "Return") return;
				else if (key == "1")
				{
					if (helping == true)
					{
						if (DidHeKilledSnowWhite() == false)
						{
							parentSession.SendText("Dont lie to me! Better hurry up!");
							return;
						}
						else 
						{
							parentSession.SendText("Buahahah. You fool, now I will kill you..\n noone can know about this..");
							parentSession.FightThisMonster(new DeamonBoss(1000));
							return;
						}
					}
					else			// first dialog
					{
						parentSession.SendText("Nice.. So please take this apple and give it to my friend SnowWhite");
						helping = true;
						visited = true;
						RunObservers(Name + "YES");
						parentSession.AddThisItem(new PoisonedApple());
						return;
					}
				}
				else if (key == "2")
				{
					if (helping == true)
					{
						parentSession.SendText("So better hurry up!");
						return;
					}
					else
					{
						parentSession.SendText("So run away fast!!!!");
						visited = true;
						return;
					}
				}
			}
		}

		private bool DidHeKilledSnowWhite()
		{
			foreach (string character in charactersVisitedByPlayer)
			{
				if (character == "interaction0006DEAD") return true;
			}
			return false;
		}
	}
}
