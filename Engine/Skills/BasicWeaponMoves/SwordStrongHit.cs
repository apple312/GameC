﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.BasicWeaponMoves
{

    [Serializable]
    class SwordStrongHit : Skill
    {
        // simple slash with sword
        public SwordStrongHit() : base("SwordStrongHit", 40, 5)
        {
            PublicName = "Strong sword hit [requires sword], a chance equal to your Precision stat to land: 1*Str damage";
            RequiredItem = "Sword";
        }
        public override List<StatPackage> BattleMove(Player player)
        {
            StatPackage response = new StatPackage("strong hit");
            
            if (Index.RNG(0, 100) < player.Precision)
            {
                response.HealthDmg = player.Strength;
                response.CustomText = "You use Strong Sword hit! (" + player.Strength + " hit damage). ";
            }
            else
            {
                response.HealthDmg = 0;
                response.CustomText = "You use Strong Sword hit, but you miss!";
            }
            return new List<StatPackage>() { response };
        }
    }
}
