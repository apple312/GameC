﻿using Game.Engine.CharacterClasses;
using Game.Engine.Skills.BasicWeaponMoves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.SkillFactories
{
	class SwordSkillsFactory : SkillFactory
	{
        // you can get all 3 basic skills in the same time, and also 1 combo 
        public Skill CreateSkill(Player player)
        {
            List<Skill> playerSkills = player.ListOfSkills;
            List<Skill> tmp = new List<Skill>();
            if (CheckForSwordSlash(playerSkills) == false) return null; //only with basic sword skill you can learn this skills

            SwordStrongHit s1 = new SwordStrongHit();
            SwordBlock s2 = new SwordBlock();
            SwordFastHit s3 = new SwordFastHit();
            if (s1.MinimumLevel <= player.Level) tmp.Add(s1);
            if (s2.MinimumLevel <= player.Level) tmp.Add(s2);
            if (s3.MinimumLevel <= player.Level) tmp.Add(s3);

            bool decoratorFlag = CheckForDecorators(playerSkills);   // cant add decorator if any of them is already learned

            if (CheckForBasic(playerSkills).Count != 0)
            {
                //for every skill player has delete itself and create possible combos
                // without combos of two times the same
                foreach (Skill skill in CheckForBasic(playerSkills))
                {
                    if (skill is SwordStrongHit)
                    {
                        tmp.Remove(s1);
                        if (decoratorFlag == false)         // cant add decorator if any of them is already learned
                        {
                            tmp.Add(new SwordFastHitDecorator(skill));
                            tmp.Add(new SwordBlockDecorator(skill));
                        }
                    }
                    if (skill is SwordBlock)
                    {
                        tmp.Remove(s2);
                        if (decoratorFlag == false)
                        {
                            tmp.Add(new SwordFastHitDecorator(skill));
                            tmp.Add(new SwordStrongHitDecorator(skill));
                        }
                    }
                    if (skill is SwordFastHit)
                    {
                        tmp.Remove(s3);
                        if (decoratorFlag == false)
                        {
                            tmp.Add(new SwordStrongHitDecorator(skill));
                            tmp.Add(new SwordBlockDecorator(skill));
                        }
                    }
                }
                if (tmp.Count == 0) return null;
                return tmp[Index.RNG(0, tmp.Count)];
            }
            else
            {
                //if player dont have any basic sword skills it will return all of them, level chcecked before
                if (tmp.Count == 0) return null;
                return tmp[Index.RNG(0, tmp.Count)]; // use Index.RNG for safe random numbers
            }

        }
       
        private List<Skill> CheckForBasic(List<Skill> skills)
        {
            List<Skill> skillsToTry = new List<Skill>();
            foreach (Skill skill in skills)
            {
                if (skill is SwordBlock || skill is SwordFastHit || skill is SwordStrongHit) skillsToTry.Add(skill);
            }

            return skillsToTry;
        }

        private bool CheckForDecorators(List<Skill> skills)
        {
            foreach (Skill skill in skills)
            {
                if (skill is SwordBlockDecorator || skill is SwordFastHitDecorator || skill is SwordStrongHitDecorator) return true;
            }
            return false;
        }

        private bool CheckForSwordSlash(List<Skill> skills)
        {
            foreach (Skill skill in skills)       //maybe there is better way to check this
            {
                if (skill is SwordSlash) return true;
            }
            return false;
        }
    }
}
