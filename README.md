*Project expanded for university course.*

**Added new iteractions (../Interactions/Added):**
* *Treasure.cs* - simple ListBox type intraction, with some random options
* *SnowWhiteAndTheSevenDwarfs* - group, connected using multi-directional observer pattern, includes:
	* *DwarfEncounter.cs* - Console type interaction, can be crated with different names, so we can have 7 unique objects in game
	* *SnowWhiteEncounter.cs* - Image type interaction
	* *MirrorEncounter.cs* - Image type interaction
	* *WitchEncounter.cs* - Image type interaction
	* *IMeetingObserver.cs* - interface needed to implement observer pattern

All this objects communicates in game, and have different options/ actions/ dialogs - which depends on player's choices with others.

**Added new iteraction factories:**
* *SnowWhiteAndTheSevenDwarfsFactory.cs* - it creates all charcters from group, makes needed communication dependencies, allow only for creating this group once for a game
* *TreasureFactory.cs* - it creates Treasures in random number with random value multiplier 

**Added new monster classes:**
* *CoronaBall.cs* - monster randomly created with stroger or weaker stats, getting stronger during fight
* *Deamon.cs* - monster with so random things in BattleMove function, also override of React function
* *DeamonBoss.cs* - just copy of Demon with stronger stats

**Added monster factories:**
* *CoronaFactory.cs* - just creating one monster in one place
* *DeamonFactory.cs* - first create Deamon object/monster, after killing it there is some chance to resp DeamonBoss

Also added images of this monsters: "*monster0003.png*", "*monster0004.png*", "*monster0005.png*". 

**Added group of items:**
* *ShortSwords.cs* - overrides ModifyOffensive function, has better effect when using with another sword type item, to implement this I needed to do list of swords and armors by engine names in added file ListOfItems.cs 
* *LongSword.cs* - overrides ApplyBuffs, change buffs depending on playeer level and other items
* *GoldSword.cs* - overrides ModifyDefensive, depending on type of damage\
* *PoisonedApple.cs* - empty item, needed for logic of interactions

**Added** *SwordFactory.cs* - creates Sword type items with different probability 
**Also added images of this items:** "*item0009.png*", "*item0010.png*", "*item0011.png*".

**Added group of skills with decorator from everyone:**
* *SwordBlock.cs / SwordBlockDecorator.cs* - no damage, it has negative stamina cost, which enable player to skip turn and get more stamina for next moves 
* *SwordStrongHit.cs / SwordStrongHitDecorator.cs* - some probability dependend on precision stat to do big damage
* *SwordFastHit.cs / SwordFastHitDecorator.cs* - small damage and also some probability depended on precision to do another effects

**Added factory for these skills** - *SwordSkillsFactory.cs* - factory enable to learn all new skills (player can have all 3 together) and only one combo skill (can choose from many)

All these elements are fully integrated, added to game.
*New images are from recommended repository: https://opengameart.org/sites/default/files/Dungeon%20Crawl%20Stone%20Soup%20Full_0.zip*
*and from:*
* *https://pixabay.com/illustrations/gothic-fantasy-dark-female-witch-1485829/*
* *https://pixabay.com/illustrations/queen-princess-pretty-beautiful-1926991/*
* *https://pixabay.com/illustrations/mystical-mirror-entwine-fantasy-404056/*
* *https://pixabay.com/illustrations/woman-female-beauty-dark-romantic-2247530/*

*- they are also free to use*