﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Items
{
	// i needed place with all engine names of items by type to my logic in items functions
	class ListOfItems
	{
		public static List<string> ListOfSwords = new List<string>()
		{
			"item0004",
			"item0009",
			"item0010",
			"item0011"
		};

		public static List<string> ListOfArmors = new List<string>()
		{
			"item0005",
			"item0006",
			"item0007",
			"item0008"
		};
	}
}
