﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.BasicWeaponMoves
{
	class SwordFastHitDecorator : SkillDecorator
	{
		public SwordFastHitDecorator(Skill skill) : base("SwordFastHit", 10, 1, skill)
		{
			MinimumLevel = Math.Max(3, skill.MinimumLevel) + 1;
			PublicName = "COMBO - Strong fast hit [requires sword], 0.2 * Precision HpDamage [hit] and a chance equal to your Precision stat / 2 to land: 0.1Strength stat StrDmg and MPdmg [complications] AND " + decoratedSkill.PublicName.Replace("COMBO: ", "");
			RequiredItem = "Sword";
		}

		public override List<StatPackage> BattleMove(Player player)
		{
            StatPackage response1 = new StatPackage("fast hit");
            response1.HealthDmg = (int)(0.2 * player.Precision);

            StatPackage response2 = new StatPackage("complications");
            if (Index.RNG(0, 100) < player.Precision / 2)
            {
                response2.StrengthDmg = (int)(0.1 * player.Strength);
                response2.MagicPowerDmg = (int)(0.1 * player.Strength);
                response1.CustomText = "You use Fast Sword hit and you was accurate enough to weak enemy! (" + (int)(0.2 * player.Precision) + " hit damage)  (" + (int)(0.1 * player.Strength) + " Str and MP dmg by complications.) . ";
            }
            else
            {
                response2.StrengthDmg = 0;
                response2.MagicPowerDmg = 0;
                response1.CustomText = "You use Fast Sword hit but you was not so accurate! (" + (int)(0.2 * player.Precision) + " hit damage)";
            }
            List<StatPackage> combo = decoratedSkill.BattleMove(player);
            combo.Add(response1);
            combo.Add(response2);
            return combo;
        }
	}
}
