﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.BasicWeaponMoves
{
	class SwordBlockDecorator : SkillDecorator
	{
		public SwordBlockDecorator(Skill skill) : base("SwordBlock", -20, 1, skill)
		{
			MinimumLevel = Math.Max(7, skill.MinimumLevel) + 2;
			PublicName = "COMBO - Sword block gives you back 20 stamina in this turn. AND " + decoratedSkill.PublicName.Replace("COMBO: ", "");
			RequiredItem = "Sword";
		}
		public override List<StatPackage> BattleMove(Player player)
		{
			StatPackage response = new StatPackage("none", 0, "I am blocking, getting back stamina");
			List<StatPackage> combo = decoratedSkill.BattleMove(player);
			combo.Add(response);
			return combo;
		}
	}
}
