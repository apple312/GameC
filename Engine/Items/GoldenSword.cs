﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Items
{
    [Serializable]
    class GoldenSword : Sword
    {
        // simple sword
        public GoldenSword() : base("item0009")
        {
            StrMod = 25;
            PrMod = 20;
            GoldValue = 100;
            StaMod = -10;    // gold is heavy so it decrease stamina
            PublicName = "Golden Sword";
        }

        // gold helps chcaracter to defense from magic
        public override StatPackage ModifyDefensive(StatPackage pack, List<string> otherItems)
        {
            if (pack.DamageType.Contains("magic") || pack.DamageType == "fire" || pack.DamageType == "water" || pack.DamageType == "air" || pack.DamageType == "earth")
            {
                pack.HealthDmg = 90 * pack.HealthDmg / 100;
                pack.MagicPowerDmg = 90 * pack.MagicPowerDmg / 100;
                pack.PrecisionDmg = 90 * pack.PrecisionDmg / 100;
                pack.StrengthDmg = 90 * pack.StrengthDmg / 100;
            }
            return pack;
        }
    }
}
