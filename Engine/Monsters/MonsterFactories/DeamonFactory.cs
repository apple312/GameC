﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Monsters.MonsterFactories
{
    [Serializable]
    class DeamonFactory : MonsterFactory
    {
        private int idNumber = 0; // needed to have diff types of deamon

        public override Monster Create(int playerLevel)
        {
            if (idNumber == 0)                  // if this is the first time, return a Rat
            {
                if (Index.RNG(-1, 3) < 0)       // there is some probability that after normal demon will resp boss
                    idNumber = 1;               // it will resp boss demon
                else
                    idNumber = 2;               // it will return null

                return new Deamon(playerLevel);
            }
            else if (idNumber == 1)
            {
                idNumber = 2;
                return new DeamonBoss(playerLevel);
            }
            else
            {
                return null; // dont have more versions of deamon, but can add similar, like fire, ice demon etc.
            }
        }
        public override System.Windows.Controls.Image Hint()
        {
            if (idNumber == 0) return new Deamon(1).GetImage();
            else if (idNumber == 1) return new DeamonBoss(1).GetImage();
            else return null;
        }
    }
}
