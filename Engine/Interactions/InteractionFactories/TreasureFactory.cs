﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Engine.Interactions.Added;

namespace Game.Engine.Interactions.InteractionFactories
{
	class TreasureFactory : InteractionFactory
	{
		public List<Interaction> CreateInteractionsGroup(GameSession parentSession)
		{
			int numberOfTreasures = Index.RNG(0, 5); // there will be 0-4 treasures in the game
			List<Interaction> tmpList = new List<Interaction>();
			for (int i = 0; i < numberOfTreasures; i++)
			{
				double multiplier = 1.0 + (double)Index.RNG(1, 11) / 10; //trasures can have diffrent values of gold/exp
				tmpList.Add(new Treasure(parentSession, multiplier));
			}
			return tmpList;
		}
	}
}
