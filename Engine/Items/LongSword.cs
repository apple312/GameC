﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Items
{
	class LongSword : Sword
	{
        public LongSword() : base("item0011")
        {
            StrMod = 30;
            ArMod = 10; // it is so big that it helps in defence
            PrMod = -10; // it is hard to be precize with long sword
            GoldValue = 70;
            StaMod = -5;    // long sword is hard to use
            PublicName = "Long Sword";
            PublicTip = "Gives nothing at first levels of player and get better when your experience in fight is bigger.";
        }

        public override void ApplyBuffs(Player currentPlayer, List<string> otherItems)
        {
            currentPlayer.StrengthBuff += StrMod + 3 * (currentPlayer.Level - 10); //using long sword works better for higher levels
            currentPlayer.ArmorBuff += ArMod + (currentPlayer.Level - 10);
            currentPlayer.PrecisionBuff += PrMod + (currentPlayer.Level - 10);

            currentPlayer.StaminaBuff += StaMod;
            foreach (var item in otherItems)
            {
                // if you are using long sword with armor you would have ever more stamina
                if (ListOfItems.ListOfArmors.Contains(item))
                {
                    currentPlayer.StaminaBuff -= 5;
                }
            }
            currentPlayer.StaminaBuff += StaMod;
            currentPlayer.MagicPowerBuff += MgcMod;
            currentPlayer.HealthBuff += HpMod;
        }
    }
}
