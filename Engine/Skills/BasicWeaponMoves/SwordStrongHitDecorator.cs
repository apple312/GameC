﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.BasicWeaponMoves
{
	class SwordStrongHitDecorator : SkillDecorator
	{
		public SwordStrongHitDecorator(Skill skill) : base("SwordStrongHit", 40, 1, skill)
		{
			MinimumLevel = Math.Max(5, skill.MinimumLevel) + 2;
			RequiredItem = "Sword";
			PublicName = "COMBO - Strong Sword Hit: a chance equal to your Precision stat to land: 1*Str damage AND " + decoratedSkill.PublicName.Replace("COMBO: ", "");
		}

		public override List<StatPackage> BattleMove(Player player)
		{
			StatPackage response = new StatPackage("strong hit");

			if (Index.RNG(0, 100) < player.Precision)
			{
				response.HealthDmg = player.Strength;
				response.CustomText = "You use Strong Sword hit! (" + player.Strength + " hit damage). ";
			}
			else
			{
				response.HealthDmg = 0;
				response.CustomText = "You use Strong Sword hit, but you miss!";
			}

			List<StatPackage> combo = decoratedSkill.BattleMove(player);
			combo.Add(response);
			return combo;
		}
	}
}
