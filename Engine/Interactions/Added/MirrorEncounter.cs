﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Engine.Interactions.InteractionFactories;

namespace Game.Engine.Interactions.Added
{
	class MirrorEncounter : ImageInteraction, IMeetingObserver
	{
		private bool visited = false;
		private List<IMeetingObserver> observers = new List<IMeetingObserver>();
		private List<string> charactersVisitedByPlayer = new List<string>();
		public MirrorEncounter(GameSession ses, params IMeetingObserver[] observers) : base(ses)
		{
			Enterable = false;
			Name = "interaction0007";
			displayedImageName = "interaction0007display";
			foreach (IMeetingObserver observer in observers)
			{
				this.observers.Add(observer);
			}
		}

		public void Receive(string name)
		{
			charactersVisitedByPlayer.Add(name);
		}

		public void RunObservers(string textToSend)
		{
			foreach (IMeetingObserver observer in observers)
			{
				observer.Receive(textToSend);
			}
		}

		public void Unsubscribe(IMeetingObserver observer)
		{
			observers.Remove(observer);
		}

		public void Subscribe(IMeetingObserver observer)
		{
			observers.Add(observer);
		}

		protected override void RunContent()
		{
			parentSession.SendText("\nWelcome! You may ask me only one question.");
			if (visited == true)
			{
				parentSession.SendText("And you did it earlier, go away!");
				return;
			}
			parentSession.SendText("You may also press ENTER to leave.\n");

			string q1 = "What should I be carefull about?";
			string q2 = "How can I earn some money and exp?";
			string q3 = "Mirror, mirror on the wall, who's the prettiest of them all?";
			string answer1 = "Be carefull of the witch.";
			string answer2 = "You should find SnowWhite and take her quest.";
			string answer3 = "Snow White is the pretties!";
			foreach (string character in charactersVisitedByPlayer)
			{
				if (character.Contains("interaction0006"))  // player met SnowWhite
				{
					if (character.Contains("YES"))          // player accepted quest from SnowWhite
					{
						q2 = "Can you help me with finding dwarfs?";
						answer2 = "Yes! I will contact you with one of them.";
					}
					else if (character.Contains("NO"))       // player didnt accept quest from Witch
					{
						answer2 = "The best way it to come back to SnowWhite and accept her ask.";
					}
				}
				else if (character.Contains("interaction0008"))  // player met Witch
				{
					if (character.Contains("YES"))          // player accepted quest from Witch
					{
						answer1 = "You should not help Witch. She is going to kill you after you kill SnowWhite..";
					}
					else if (character.Contains("NO"))       // player didnt accept quest from Witch
					{
						answer1 = "Never come back to the witch. She would kill you!";
					}
				}
			}

			parentSession.SendText(q1+" (press 1)");
			parentSession.SendText(q2+" (press 2)");
			parentSession.SendText(q3+" (press 3)");
			while (true)
			{
				string key = parentSession.GetValidKeyResponse(new List<string>() { "Return", "1", "2", "3" }).Item1;
				if (key == "Return") return;
				else if (key == "1")
				{
					parentSession.SendText(answer1);
				}
				else if (key == "2")
				{
					if (q2 == "Can you help me with finding dwarfs?") //check if mirror can help with quest
					{
						parentSession.SendText(answer2);
						foreach (string dwarf in SnowWhiteAndTheSevenDwarfsFactory.sevenDwarfsNames)
						{
							// check if player visited dwarfs if not or if before accepting quest from SnowWhite - find dwarf to player
							if (!(charactersVisitedByPlayer.Contains(dwarf + "NO") || charactersVisitedByPlayer.Contains(dwarf)))
							{
								RunObservers(Name + dwarf);
								return;
							}
						}
					}
					else parentSession.SendText(answer2);			// mirror cant help with dwarf because we didnt accept SnowWhite quest earlier

				}
				else if (key == "3")
				{
					parentSession.SendText(answer3);
				}
				visited = true;
				return;
			}
		}
	}
}
