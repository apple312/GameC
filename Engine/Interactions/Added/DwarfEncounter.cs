﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Interactions.Added
{
	class DwarfEncounter : ConsoleInteraction, IMeetingObserver
	{
		private bool foundForSnowWhite = false;
		private bool metByMirror = false;
		private List<IMeetingObserver> observers = new List<IMeetingObserver>();
		private List<string> charactersVisitedByPlayer = new List<string>();
		public DwarfEncounter(GameSession ses, string name, params IMeetingObserver[] observers) : base(ses)
		{
			Name = name;
			Enterable = false;
			foreach (IMeetingObserver observer in observers)
			{
				this.observers.Add(observer);
			}
		}

		public void Receive(string name)
		{
			if (name.Contains("interaction0007"))
			{
				if (name.Contains(Name))
				{
					RunObservers(Name + "YES");
					metByMirror = true;
					foundForSnowWhite = true;
				}
			}
			charactersVisitedByPlayer.Add(name);
			//if from mirror that mean you are founded and should send this to observers and chand your status
		}

		public void RunObservers(string textToSend)
		{
			foreach (IMeetingObserver observer in observers)
			{
				observer.Receive(textToSend);
			}
		}

		public void Unsubscribe(IMeetingObserver observer)
		{
			observers.Remove(observer);
		}

		public void Subscribe(IMeetingObserver observer)
		{
			observers.Add(observer);
		}

		protected override void RunContent()
		{
			foreach (string character in charactersVisitedByPlayer)
			{
				if (character.Contains("interaction0006"))	// player met SnowWhite
				{
					if (character.Contains("YES"))          // player accepted quest from SnowWhite
					{
						if (foundForSnowWhite == false)                 // player havent met this dwarf earlier (but after meeting SnowWhite) 
						{
							parentSession.SendText("Hello, I see you talked with SnowWhite, I wil go with you");
							RunObservers(Name + "YES");     //inform other about this talk
							foundForSnowWhite = true;
						}
						else
						{
							if (metByMirror) parentSession.SendText("We have met by mirror. Take us all to SnowWhite please!");
							else parentSession.SendText("We have met. Take us all to SnowWhite please!");
						}
					}
					else if (character.Contains("DEAD"))
					{
						parentSession.SendText("You are killer!!! I will beat you. [you are cripple now - minus 50% of HP");
						parentSession.UpdateStat(1, -(parentSession.currentPlayer.Health / 2));
					}

				}
			}
			if (foundForSnowWhite == false)
			{
				parentSession.SendText("I hate people - go away!");
				RunObservers(Name + "NO");
			}
		}
	}
}
