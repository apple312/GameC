﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Engine.Interactions.Built_In;
using Game.Engine.Monsters;

namespace Game.Engine.Interactions.Added
{
	class Treasure : ListBoxInteraction
	{
		private int levelNeededForNextVisit = 1;
		private double valueMultiplier = 1;
		public Treasure(GameSession ses, double multiplier) : base(ses)
		{
			Enterable = false;
			Name = "interaction0005";
			valueMultiplier = multiplier;
		}

		protected override void RunContent()
		{
			parentSession.SendText("Great! You found treasure and you need to choose something. ");
			if (parentSession.currentPlayer.Level >= levelNeededForNextVisit)
			{
				int choice = GetListBoxChoice(new List<string> { "I want gold!", "I want exp.", "I want everything (some chances to get nothing)" });
				if (choice == 0)
				{
					parentSession.SendText("You you can take some money. Come back after you get +5 levels.");
					parentSession.UpdateStat(8, parentSession.currentPlayer.Level * 100 - 50);
				}
				else if (choice == 1)
				{
					parentSession.SendText("You you will have extra exp after winning this fight. Come back after you get +5 levels.");
					parentSession.FightThisMonster(new Rat(parentSession.currentPlayer.Level));
					parentSession.UpdateStat(7, parentSession.currentPlayer.Level * 50 - 15);
				}
				else
				{
					if (Index.RNG(-2,3) < 0)
					{
						parentSession.SendText("You are lucky, you can take everything. Come back after you get +5 levels.");
						parentSession.UpdateStat(8, parentSession.currentPlayer.Level * 100 - 50);
						parentSession.UpdateStat(7, parentSession.currentPlayer.Level * 50 - 15);
					}
					else
					{
						parentSession.SendText("You are unlucky and dont get anything, but you can come back after you get +5 levels.");
					}
				}
				levelNeededForNextVisit += 5;
			}
			else
			{
				parentSession.SendText("Unfortunately it is looted already. Come back on lvl " + levelNeededForNextVisit + " or higher.");
			}
		}
	}
}
