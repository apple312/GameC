﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Interactions.Added
{
	interface IMeetingObserver
	{
		void Subscribe(IMeetingObserver observer);
		void Unsubscribe(IMeetingObserver observer);
		void RunObservers(string textToSend);		//name because of using "Run" in program
		void Receive(string name);
	}
}
