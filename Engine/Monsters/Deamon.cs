﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Monsters
{
    [Serializable]
    class Deamon : Monster
    {
        private bool defenceFlag = false;
        public Deamon(int demonLevel)
        {
            Health = 100 + 3 * demonLevel;
            Strength = 20 + demonLevel;
            Armor = 15;
            Precision = 50;
            MagicPower = 20;
            Stamina = 30;
            XPValue = 75 + 2 * demonLevel;
            Name = "monster0004";
            BattleGreetings = " I am deamon, you should be afraid!";
        }
        public override List<StatPackage> BattleMove()
        {
            if (Stamina > 0)
            {
                // in move after defence armor should be bak to normal
                if (defenceFlag == true)
                {
                    Armor = 15;
                    defenceFlag = false;
                }

                Stamina -= 10;
                // if deamon is lucky it gets critical attack and decrease players armor and magic power
                if (Index.RNG(0, 100) < precision)
                {
                    return new List<StatPackage>()
                    {
                        new StatPackage("magic critical stab", 2 * Strength + MagicPower, 0, 10, 0, MagicPower, 
                                        "Magic Crittical hit (" + (2 * Strength + MagicPower) + "damage) ( " + 10 + " armor decrease) ( " + MagicPower + " magic power decrease)" )
                    };
                }
                else
                {
                    return new List<StatPackage>()
                    { 
                        new StatPackage("magic stab", 2 * Strength + MagicPower,
                                        "Magic hit (" + (2 * Strength + MagicPower) + "damage)" ) 
                    };
                }
                
            }
            else
            {
                // when deamon will not have enough stamina it would do defence and rest
                defenceFlag = true;
                Armor += 15;
                Stamina += 30;
                return new List<StatPackage>() { new StatPackage("none", 0, "Demon in defence, gaining armor and stamina...") };
            }
        }

        public override void React(List<StatPackage> packs)
        {
            foreach (StatPackage pack in packs)
            {
                if (pack.HealthDmg - Armor > 0)
                    Health -= (pack.HealthDmg - Armor);
                Strength -= pack.StrengthDmg;
                Armor -= pack.ArmorDmg;
                Precision -= pack.PrecisionDmg;
                MagicPower -= pack.MagicPowerDmg;
            }
        }
    }
}
