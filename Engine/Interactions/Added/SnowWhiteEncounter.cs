﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Game.Engine.Items;
using Game.Engine.Interactions.InteractionFactories;

namespace Game.Engine.Interactions.Added
{
	class SnowWhiteEncounter : ImageInteraction, IMeetingObserver
	{
		private bool dead = false;
		private bool helping = false;
		private bool collectedReward = false;
		private List<IMeetingObserver> observers = new List<IMeetingObserver>();
		private List<string> charactersVisitedByPlayer = new List<string>();
		public SnowWhiteEncounter(GameSession ses, params IMeetingObserver[] observers) : base(ses)
		{
			Enterable = false;
			Name = "interaction0006";
			displayedImageName = "interaction0006display";
			foreach (IMeetingObserver observer in observers)
			{
				this.observers.Add(observer);
			}
		}

		public void Receive(string name)
		{
			charactersVisitedByPlayer.Add(name);
		}

		public void RunObservers(string textToSend)
		{
			foreach (IMeetingObserver observer in observers)
			{
				observer.Receive(textToSend);
			}
		}

		public void Unsubscribe(IMeetingObserver observer)
		{
			observers.Remove(observer);
		}

		public void Subscribe(IMeetingObserver observer)
		{
			observers.Add(observer);
		}

		protected override void RunContent()
		{
			if (dead == false)
			{
				if (helping == false)
				{
					parentSession.SendText("Welcome, traveler! I am SnowWhite.");
					parentSession.SendText("I have lost all my dwarfs.. I am so tired and hungry.. Can you help me with finding them?");
					string answer1 = "Yes, sure!";
					string answer2 = "No thanks.";
					if (parentSession.TestForItem("item0012"))
					{
						answer2 = "I dont have enough time but I will be back, for now I can give you this apple.";
					}

					parentSession.SendText(answer1 + " (press 1)");
					parentSession.SendText(answer2 + " (press 2)");
					parentSession.SendText("ENTER to close");

					while (true)
					{
						string key = parentSession.GetValidKeyResponse(new List<string>() { "Return", "1", "2" }).Item1;
						if (key == "Return") return;
						else if (key == "1")
						{
							parentSession.SendText("Thank you so much. Please find my dwarfs and come back to me \n I will give you reward!");
							helping = true;
							RunObservers(Name + "YES");
							return;
						}
						else if (key == "2")
						{
							if (answer2.Contains("apple"))
							{
								parentSession.SendText("Oh no! Heeeelp...");
								displayedImageName = "interaction0006display1";
								dead = true;
								RunObservers(Name + "DEAD");
								return;
							}
							else
							{
								parentSession.SendText("Meeh, but please come back if you will have more time.");
								return;
							}
						}
					}
				}
				else				//helping her already
				{
					if (collectedReward == true)
					{
						parentSession.SendText("Nice to see you again my hero!");
						return;
					}
					else parentSession.SendText("Hello, nice to see you again. Did you find them?");
					string answer1 = "Yes, at least some of them. [check how much more you need]";
					string answer2 = "Not yet.";
					if (parentSession.TestForItem("item0012"))
					{
						answer2 = "But for now I can give you this apple.";
					}

					parentSession.SendText(answer1 + " (press 1)");
					parentSession.SendText(answer2 + " (press 2)");
					parentSession.SendText("ENTER to close");

					while (true)
					{
						string key = parentSession.GetValidKeyResponse(new List<string>() { "Return", "1", "2" }).Item1;
						if (key == "Return") return;
						else if (key == "1")
						{
							int foundDwarfs = CheckDwarfs();
							if (collectedReward == false && foundDwarfs == 7)
							{
								parentSession.SendText("Wow, you found them all, amazing, there is your reward:");
								parentSession.SendText("+1000gold, +1000exp");
								parentSession.UpdateStat(8, 1000);
								parentSession.UpdateStat(7, 1000);
								collectedReward = true;
								return;
							}
							else
							{
								parentSession.SendText("You have only " + foundDwarfs + "/7. Come back later..");
								return;
							}
						}
						else if (key == "2")
						{
							if (answer2.Contains("apple"))
							{
								RunObservers(Name + "DEAD");
								parentSession.SendText("Oh no! Heeeelp...");
								displayedImageName = "interaction0006display1";
								dead = true;
								return;
							}
							else
							{
								parentSession.SendText("Meeh, but please come back if you will have more time.");
								return;
							}
						}
					}

				}
			}
			else
			{
				parentSession.SendText("Snow white in dead sleep...( press ENTER to close image)");
				while (true)
				{
					string key = parentSession.GetValidKeyResponse(new List<string>() { "Return" }).Item1;
					if (key == "Return") return;
				}
			}
		}

		private int CheckDwarfs()
		{
			int foundDwarfs = 0;
			foreach (string character in charactersVisitedByPlayer)
			{
				foreach (string dwarfName in SnowWhiteAndTheSevenDwarfsFactory.sevenDwarfsNames)
				{
					if (character == dwarfName + "YES") foundDwarfs++;
				}
			}
			return foundDwarfs;
		}
	}
}
