﻿using Game.Engine.CharacterClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Skills.BasicWeaponMoves
{
	class SwordFastHit : Skill
	{
        // simple slash with sword
        public SwordFastHit() : base("SwordFastHit", 30, 3)
        {
            PublicName = "Strong fast hit [requires sword], 0.2 * Precision HpDamage [hit] and a chance equal to your Precision stat / 2 to land: 0.1Strength stat StrDmg and MPdmg [complications]";
            RequiredItem = "Sword";
        }
        public override List<StatPackage> BattleMove(Player player)
        {
            StatPackage response1 = new StatPackage("fast hit");
            response1.HealthDmg = (int)(0.2 * player.Precision);

            StatPackage response2 = new StatPackage("complications");
            if (Index.RNG(0, 100) < player.Precision / 2)
            {
                response2.StrengthDmg = (int)(0.1 * player.Strength);
                response2.MagicPowerDmg = (int)(0.1 * player.Strength);
                response1.CustomText = "You use Fast Sword hit and you was accurate enough to weak enemy! (" + (int)(0.2 * player.Precision) + " hit damage)  (" + (int)(0.1 * player.Strength) + " Str and MP dmg by complications.) . ";
            }
            else
            {
                response2.StrengthDmg = 0;
                response2.MagicPowerDmg = 0;
                response1.CustomText = "You use Fast Sword hit but you was not so accurate! (" + (int)(0.2 * player.Precision) + " hit damage)";
            }
            return new List<StatPackage>() { response1, response2 };
        }
    }
}
