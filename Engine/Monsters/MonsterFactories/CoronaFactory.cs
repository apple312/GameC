﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Game.Engine.Monsters.MonsterFactories
{
	class CoronaFactory : MonsterFactory
	{
		private int encounterNumber = 0;

		public override Monster Create(int playerLevel)
		{
			if (encounterNumber == 0)
			{
				encounterNumber++;
				return new CoronaBall(playerLevel);
			}
			else return null;
		}

		public override System.Windows.Controls.Image Hint()
		{
			if (encounterNumber == 0)
				return new CoronaBall(1).GetImage();
			else
				return null;
		}
	}
}
