﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game.Engine.Items.ItemFactories
{
	[Serializable]
	class SwordFactory : ItemFactory
	{
		public Item CreateItem()
		{
			int rarity = Index.RNG(0, 22);
			if (rarity < 10) return new BasicSword();
			else if (rarity < 15) return new ShortSword();
			else if (rarity < 20) return new LongSword();
			else return new GoldenSword();
		}

		public Item CreateNonMagicItem()
		{
			return CreateItem();
		}

		public Item CreateNonWeaponItem()
		{
			return null;
		}
	}
}
